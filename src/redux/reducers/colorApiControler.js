let initialState ={
  color : '#5d8e9b'
}


export  const colorApiControler = (state=initialState,action) => {
  switch (action.type) {
    case 'COLOR_API': return {
      color : action.color
    }

    default: return state;
      
  }

}