import { combineReducers } from 'redux'
import {UrlControler} from './urlControler'
import {LangControler} from './changeLang'
import {WidthControler} from './widthControler'
import {ThemeControler} from './themeControler'
import {colorApiControler} from './colorApiControler'
import {getTokenControler} from './getTokenControler'


export const RootReducers = combineReducers({
  url : UrlControler,
  langDir : LangControler,
  width : WidthControler,
  theme : ThemeControler,
  colorApi : colorApiControler,
  token : getTokenControler
})