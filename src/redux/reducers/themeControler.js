let initialState ={
  theme : "light"
}


export  const ThemeControler = (state=initialState,action) => {
  switch (action.type) {
    case 'CHANGETHEME': 
    let {theme} = state;

    if(theme === "light") {
      theme = "dark"
    }
    else {
      theme = "light"
    }
    
    return {
      ...state,
      theme : theme
    }
      
    default: return state;
      
  }

}