let initialState ={
  language : "FA",
  direction : "rtl"

}


export  const LangControler = (state=initialState,action) => {
  switch (action.type) {
    case 'CHANGELANG': {
    let {language} = state;
    let {direction} = state;

    if(language === "FA") {
      language = "EN"
      direction= "ltr"
    }
    else{
      language="FA"
      direction="rtl"
    }

    return {
      ...state,
      language,
      direction
    }

 }
     
    default:
      return state;
      
 } 
}
