let initialState ={
  width : 2048
}


export  const WidthControler = (state=initialState,action) => {
  switch (action.type) {
    case 'CHECKWIDTH': return {
      width : action.width
    }
      
    default: return state;
      
  }

}