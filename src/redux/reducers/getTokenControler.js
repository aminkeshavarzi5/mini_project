import * as ACTION_TYPE from './../actions/index'
let initialState ={
  token : null
}


export  const getTokenControler = (state=initialState,action) => {
  switch (action.type) {
    case 'GET_TOKEN': 
    return {
      token : action.res
    }
    case ACTION_TYPE.CLEAR_TOKEN: 
    return {
      token : null
    }
    default: return state;
      
  }

}