import { createBrowserHistory } from 'history';

let initialState ={
  url : createBrowserHistory().location.pathname
}

export  const UrlControler = (state=initialState,action) => {
  switch (action.type) {
    case 'CHANGEURLPATH': return {
      url : action.url
    }
      
    default: return state;
      
  }

}