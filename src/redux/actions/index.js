import axios from 'axios'

export const changeUrlPath = "CHANGEURLPATH"
export const changelANG = 'CHANGELANG'
export const checkWidthPage = 'CHECKWIDTH'
export const changeThemePage = 'CHANGETHEME'
export const colorApi = 'COLOR_API'
export const GetTokens = 'GET_TOKEN'
export const CLEAR_TOKEN = 'CLEAR_TOKEN'

export const getToken = (res) =>  ({
      type : GetTokens,
      res

})

export const clearToken = () =>  ({
  type : CLEAR_TOKEN
})
   


export const loadColorApi = () =>{
  return (dispatch) => {
      axios.get("http://www.colr.org/json/color/random").then((response) => {
        dispatch(changeColorApi("#"+response.data.new_color))
      })
  }
}

export const changeColorApi = (color) => {
    return {
      type : colorApi,
      color : color
    }
}


export const updatePathname = (location) => {
  return {
    type : changeUrlPath,
    url : location
  }
}

export const changeLanguage = () => {
  return {
    type : changelANG

  }
}

export const checkWidth = (width) => {
  return {
    type : checkWidthPage,
    width : width
  }
}

export const changeTheme = () => {
  return {
    type : changeThemePage

  }
}


