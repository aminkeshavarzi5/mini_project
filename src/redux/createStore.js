import { createStore, applyMiddleware } from "redux"
import {
  persistCombineReducers,
  persistReducer,
  persistStore,
} from "redux-persist"
import storage from "localforage"
import { composeWithDevTools } from "redux-devtools-extension"
import thunk from "redux-thunk"
import { routerMiddleware } from "react-router-redux"
import { createMemoryHistory as createHistory } from "history"

import {UrlControler} from './reducers/urlControler'
import {LangControler} from './reducers/changeLang'
import {WidthControler} from './reducers/widthControler'
import {ThemeControler} from './reducers/themeControler'
import {colorApiControler} from './reducers/colorApiControler'
import {getTokenControler} from './reducers/getTokenControler'

const rootPersistConfig = {
  key: "root",
  storage,
  blacklist: ["navbar", "card"],
  // debug: true,
}
const rootReducer = persistCombineReducers(rootPersistConfig, {
  url: persistReducer(
    {
      key: "url",
      storage,
      blacklist: [],
    },
    UrlControler
  ),
  langDir: persistReducer(
    {
      key: "langDir",
      storage,
      blacklist: [],
    },
    LangControler
  ),
  width: persistReducer(
    {
      key: "width",
      storage,
      blacklist: [],
    },
    WidthControler
  ),
  theme: persistReducer(
    {
      key: "theme",
      storage,
      blacklist: [],
    },
    ThemeControler
  ),
  colorApi: persistReducer(
    {
      key: "colorApi",
      storage,
      blacklist: [],
    },
    colorApiControler
  ),
  token: persistReducer(
    {
      key: "token",
      storage,
      blacklist: [],
    },
    getTokenControler
  ),
})

const history = createHistory()
export { history }
export const configureStore = () => {
  const store = createStore(
    rootReducer,
    undefined,
    composeWithDevTools(applyMiddleware(thunk, routerMiddleware(history)))
  )
  const persistor = persistStore(store)
  return { persistor, store }
}
