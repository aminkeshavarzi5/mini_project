import React, { useEffect } from "react";
import Routes from "./router";
import { updatePathname } from "./redux/actions";
import { useDispatch, useSelector } from "react-redux";
import { withRouter } from "react-router-dom";
import { checkWidth, clearToken } from "./redux/actions";
import axios from "axios";
import { GoogleReCaptchaProvider } from "react-google-recaptcha-v3";
const App = ({ history }) => {
  const dispatch = useDispatch();
  const token = useSelector(state => state.token.token);

  useEffect(() => {
    history.listen(location => {
      dispatch(updatePathname(location.pathname));
    });
    window.addEventListener("resize", () =>
      dispatch(checkWidth(window.innerWidth))
    );
  }, [dispatch, history]);

  useEffect(() => {
    const getUser = async () => {
      await axios.post("http://localhost:8888/wp-json/wp/v2/users/me");
    };
    if (!!token) {
      axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    } else {
      delete axios.defaults.headers.common["Authorization"];
    }
    axios.interceptors.response.use(undefined, function(error) {
      if (error.response.status === 403) {
        dispatch(clearToken());
      } else if (error.response.status === 401) {
        throw error;
      }
    });
    getUser();
    // eslint-disable-next-line
  }, [token]);
  const locale = useSelector(state => state.langDir.language);
  return (
    <>
      <GoogleReCaptchaProvider
        language={locale.toLowerCase()}
        reCaptchaKey="6LdEctkUAAAAAE9UqscZO77WXu_zkz53ZkYBodN2"
      >
        <Routes />
      </GoogleReCaptchaProvider>
    </>
  );
};

export default withRouter(App);
