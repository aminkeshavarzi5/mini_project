import React from 'react'
import { Route, Switch } from "react-router-dom";
import Loadable from "react-loadable";
import CircularProgress from '@material-ui/core/CircularProgress';



const Loading = () => {
  return (
    <div style={{height:'90vh', width:'100%',display:'flex',alignItems:'center',justifyContent:'center'}}>
        <CircularProgress style={{color:"#ff9800"}}/>
    </div>
  )
}

const Home = Loadable({
  loader: () => import("./components/home/"),
  loading: Loading
});
const Contact = Loadable({
  loader: () => import("./components/contact/"),
  loading: Loading
});
const About = Loadable({
  loader: () => import("./components/about/index"),
  loading: Loading
});
const Article = Loadable({
  loader: () => import("./components/article/"),
  loading: Loading
});
const Navbar = Loadable({
  loader: () => import("./components/navbar/"),
  loading: Loading
});
const Footer = Loadable({
  loader: () => import("./components/footer/"),
  loading: Loading
});
const MainPost = Loadable({
  loader: () => import("./components/posts"),
  loading: Loading
});
const Reddit = Loadable({
  loader: () => import("./components/reddit/"),
  loading: Loading
});
const Users = Loadable({
  loader: () => import("./components/users/"),
  loading: Loading
});



const Routes = () => {




  return (
    <>
    <Navbar/>
    <div style={{backgroundColor:'#5d8e9b', minHeight: "calc(100vh - 254px)" }}>
    <Switch>
    <Route path="/" exact component={Home} />
    <Route path="/contact" component={Contact} />
    <Route path="/about" component={About} />
    <Route path="/article" component={Article} />
    <Route path="/posts" component={MainPost} />
    <Route path="/reddit" component={Reddit} />
    <Route path="/users" component={Users} />



    </Switch> 
    </div>
    <Footer/>
    </>
   
  )
}

export default Routes;
