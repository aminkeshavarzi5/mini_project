import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import wave from "./assets/wave3.png";
import background from "./assets/background_about.jpg";
import "./style.scss";
import { Typography } from "@material-ui/core";

const About = () => {
  const classes = useStyles();

  useEffect(() => {
    const text = document.querySelector("#fancyTxt");
    const textStr = text.textContent;
    const splitText = textStr.split("");
    text.innerHTML = "";

    for (let i = 0; i < splitText.length; i++) {
      text.innerHTML += "<span>" + splitText[i] + "</span>";
    }
    
    let char = 0;
    const onTick = () => {
      const span = text.querySelectorAll("span")[char];
      span.classList.add("fade");
      char++;
      if (char === splitText.length) {
        complete();
        return;
      }
    };

    const complete = () => {
      clearInterval(timer);
      timer = null;
    };

    let timer = setInterval(onTick, 50);
  }, []);

  return (
    <div className={classes.container}>
      <div className={classes.topSection}>
        <img
          src={background}
          alt="background"
          style={{
            maxHeight: 700,
            width: "100%",
            height: "100%",
            opacity: "0.6"
          }}
        />
        <div className={classes.wave} />

        <div className={classes.titleText}>
          <h1 id="fancyTxt">"about our services"</h1>
        </div>
      </div>
      <div className={classes.details}>
            <div className={classes.sections}>
              <Typography className={classes.titleSections}>service number 1</Typography>
              <Typography className={classes.paragraphSections}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </Typography>
            </div>
            <div className={classes.sections}>
              <Typography className={classes.titleSections}>service number 2</Typography>
              <Typography className={classes.paragraphSections}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </Typography>
            </div>
            <div className={classes.sections}>
              <Typography className={classes.titleSections} >service number 3</Typography>
              <Typography className={classes.paragraphSections}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </Typography>
            </div>
            <div className={classes.sections}>
              <Typography className={classes.titleSections}>service number 4</Typography>
              <Typography className={classes.paragraphSections}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</Typography>
            </div>
      </div>
    </div>
  );
};

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexDirection:'column',
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    background:'black'
    
  },
  topSection: {
    display: "flex",
    width: "100%",
    position: "relative",
    maxHeight: 700,
    
  },
  titleText: {
    width: "100%",
    position: "absolute",
    top: "20%",
    bottom: "80%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  wave: {
    position: "absolute",
    width: "100%",
    height: 143,
    bottom: 0,
    left: 0,
    display:'inline-table',
    animation: "animate 10s linear infinite",
    backgroundImage: `url(${wave})`,
    "&:before": {
      content: '""',
      width: "100%",
      height: 143,
      backgroundImage: `url(${wave})`,
      position: "absolute",
      display:'inline-table',
      top: 0,
      left: 0,
      opacity: "0.4",
      animation: "animate-reverse 10s linear infinite"
    },
    "&:after": {
      content: '""',
      width: "100%",
      height: 143,
      backgroundImage: `url(${wave})`,
      position: "absolute",
      top: 0,
      left: 0,
      display:'inline-table',
      opacity: "0.6",
      animationDelay: "-5s",
      animation: "animate 20s linear infinite"
    }
  },
  details:{
    width:'70%',
    display:'grid',
    gridGap:10,
    gridTemplateColumns: "repeat(2, minmax(170px, 1fr))",
    justifyContent:'center',
    alignItems:'center',
    marginTop:200,
    marginBottom:50
  },
  sections:{
    width:'100%',
    display:'flex',
    flexDirection:'column',
    justifyContent:'center',
    alignItems:'center',
    color:'white',
    border:'2px solid white',
    borderRadius:'40px',
    padding:"20px 50px",
    minHeight:240,
    textAlign: "center",
   
  },
  titleSections:{
    fontSize:'1.5em',
    fontWeight:'bold'
  },

}));

export default About;
