import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography, Box, Button } from "@material-ui/core";
import milkshake from "./assets/milkshake.jpg";
import latte from "./assets/latte.jpg";
import Cappuccino from "./assets/Cappuccino.jpg";
// import { Link } from "react-router-dom";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import AwesomeSlider from "react-awesome-slider";
import "react-awesome-slider/src/styled/fall-animation";
import styles from "./styles.scss";
import morty1 from "./assets/rick-and-morty1.jpg";
import morty2 from "./assets/rick-and-morty2.jpg";

import morty5 from "./assets/rick-and-morty5.jpg";

const Home = () => {
  const classes = useStyles();

  const slider = (
    <AwesomeSlider animation="fallAnimation" cssModule={styles}>
      <div data-src={morty1} className={classes.imgSlopacity} />

      <div data-src={morty2} />

      <div data-src={morty5} />
    </AwesomeSlider>
  );

  return (
    <div className={classes.container}>
      <div className={classes.optionNavbar}>
        <ul
          className={classes.ulNav}
          style={{ listStyleType: "none", padding: 0 }}
        >
         
            <li>
              <Button className={classes.btns}>Cappuccino</Button>
            </li>
         
         
            <li>
              <Button className={classes.btns}>Latte</Button>
            </li>
         
            <li>
              <Button className={classes.btns}>milkshake</Button>
            </li>
          
        </ul>
      </div>

      <div className={classes.slider}>
        <div className={classes.sliderSection}>{slider}</div>
        <Box className={classes.mainText}>
          <Typography className={classes.mainTitleTxt}>
            Stylish Coffee Shop
          </Typography>
          <Typography className={classes.txts}>
            WELCOME! WE ARE CURRENTLY
          </Typography>
          <Typography className={classes.txts}>SERVING LUNCH!</Typography>
          <Typography className={classes.txts}>CARE TO SEE A MENU?</Typography>
        </Box>
        <Button variant="contained" className={classes.bookBtn}>
          LUNCH MENU
        </Button>
        <div className={classes.learnMore}>
          <Typography className={classes.learnTxt}>LERN MORE</Typography>
          <KeyboardArrowDownIcon className={classes.arrow} />
        </div>
      </div>

      <div className={classes.Sections} style={{ marginTop: 200 }}>
        <img
          style={{ width: "100%", opacity: "0.5" }}
          src={Cappuccino}
          alt="Cappuccino"
        />
        <div className={classes.innerSections}>
          <Typography className={classes.orderTxt}>
            Order our sweet delisous Cappuccino{" "}
          </Typography>
          <Button className={classes.orderBtn} variant="contained">
            Order now
          </Button>
        </div>
      </div>
      <div className={classes.Sections}>
        <img
          style={{ width: "100%", height: "100%", opacity: "0.5" }}
          src={latte}
          alt="latte"
        />
        <div className={classes.innerSections}>
          <Typography className={classes.orderTxt}>
            Order our sweet delisous latte{" "}
          </Typography>
          <Button className={classes.orderBtn} variant="contained">
            Order now
          </Button>
        </div>
      </div>
      <div className={classes.Sections}>
        <img
          style={{ width: "100%", height: "100%", opacity: "0.5" }}
          src={milkshake}
          alt="milkshake"
        />
        <div className={classes.innerSections}>
          <Typography className={classes.orderTxt}>
            Order our sweet delisous milkshake{" "}
          </Typography>
          <Button className={classes.orderBtn} variant="contained">
            Order now
          </Button>
        </div>
      </div>
    </div>
  );
};

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    width: "100%"
  },
  optionNavbar: {
    background: "linear-gradient(to right top,#e96443,#904e95)",
    top: 0,
    zIndex: 2,
    height: 75,
    display: "flex",
    width: "100%",
    boxShadow: "0px 5px 10px rgba(0,0,0,0.8)",
    justifyContent: "center",
    alignItems: "center",
    position: "sticky"
  },
  ulNav: {
    display: "flex",
    fontSize: "1.7em",
    width: "40%",
    justifyContent: "space-between"
  },
  btns: {
    color: "white",
    fontSize: "0.7em",

    padding: 10,
    "&:hover": {
      color: "black"
    }
  },
  slider: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    position: "relative"
    // height: 700
  },
  sliderSection: {
    opacity: "0.7",
    width: "100%",
    height: "100%"
  },
  mainText: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    width: "60%",
    position: "absolute",
    bottom: "70%",
    top: "30%"
  },
  mainTitleTxt: {
    fontSize: "3em",
    color: "#ff9800"
  },
  txts: {
    fontSize: "2em",
    marginTop: 10
  },
  bookBtn: {
    fontSize: "1.1em",
    color: "white",
    position: "absolute",
    top: "50%",
    width: "20%",
    height: "50px",
    bottom: "30%",
    backgroundColor: "#c2185b",
    "&:hover": {
      color: "black",
      backgroundColor: "#ff9800"
    }
  },
  learnMore: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    width: "30%",
    position: "absolute",
    bottom: "10%",
    top: "90%"
  },
  learnTxt: {
    fontSize: "1.1em",
    backgroundColor: "#ff9800",
    borderRadius: "40px",
    padding: 10
  },
  arrow: {
    fontSize: "2em",
    color: "white",
    marginTop: 10,
    "&:hover": {
      color: "black",
      backgroundColor: "white",
      borderRadius: "40px"
    }
  },
  Sections: {
    display: "flex",
    flexDirection: "column",
    width: "70%",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 30,
    position: "relative"
  },
  innerSections: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    top: "50%",
    bottom: "50%"
  },
  orderTxt: {
    fontSize: "2em",
    marginBottom: 15
  },
  orderBtn: {
    backgroundColor: "#ff9800",
    width: "20%",
    borderRadius: "40px",
    "&:hover": {
      color: "white",
      backgroundColor: "#c2185b"
    }
  }
}));

export default Home;
