import React, { useEffect } from "react";
import RedditApi from "./redditApi";
import { makeStyles } from "@material-ui/core/styles";
import redditGuy from './assets/redditGuy.jpg'

const Reddit = () => {
 
  useEffect(() => {
    const searchForm = document.getElementById("search-form");
    const searchInput = document.getElementById("search-input");

    //Form event listener
    searchForm.addEventListener("submit", e => {
      e.preventDefault();
      //Get search term
      const searchTerm = searchInput.value;

      //Get sort
      const sortBy = document.querySelector('input[name="sortby"]:checked')
        .value;
      // console.log(sortBy);

      //Get limit
      const searchLimit = document.getElementById("limit").value;

      //check input
      if (searchTerm === "") {
        //show message
        showMessage("please add a search term", "alert-danger");
      }
      //Clear input
      searchInput.value = "";

      //search Reddit
      RedditApi.search(searchTerm, searchLimit, sortBy).then(results => {
        
        let outPut = '<div class="card-columns">';
        //Loop through posts
        results.forEach(post => {
          const image = post.preview ? post.preview.images[0].source.url : redditGuy
          
          outPut += `
        <div class="card">
          <img src="${image}" class="card-img-top" alt=${image}>
          <div class="card-body">
            <h5 class="card-title">${truncateText(post.title,100)}</h5>
            <p class="card-text">${truncateText(post.selftext,100)}</p>
            <a href="${post.url}" target="_blank" class="btn btn-primary">read More</a>
            </hr>
            <span class="badge badge-secondary "> Subreddit: ${post.subreddit} </span>
            <span class="badge badge-dark "> Score: ${post.score} </span>
          </div>
        </div>`  
         
        });
        outPut += '</div>'
        document.getElementById('results').innerHTML = outPut;
      });
    });

    const showMessage = (message, className) => {
      //create div
      const div = document.createElement("div");
      //add classes
      div.className = `alert ${className}`;
      //add text
      div.appendChild(document.createTextNode(message));
      //Get parent
      const searchContainer = document.getElementById("search-container");
      //Get search
      const search = document.getElementById("search");

      //Insert message
      searchContainer.insertBefore(div, search);

      // Timeout alert
      setTimeout(() => document.querySelector(".alert").remove(), 3000);
    };


    //truncate text
    const truncateText = (text, limit) => {
      const shortend = text.indexOf(' ',limit);
      if(shortend === -1) return text;
      return text.substring(0,shortend);
       
    }
  }, []);

  const useStyles = makeStyles(theme => ({
      container:{
        width:'100%',
        alignItems:'center',
        justifyContent:'center',
      }
    }));
    const classes = useStyles();

  return (
    <div className={classes.container} >
      <nav className="navbar navbar-dark bg-primary mb-3">
        <div className="container">
          <span className="navbar-brand">Finddit</span>
        </div>
      </nav>
      <div id="search-container" className="container">
        <div id="search" className="card card-body bg-light mb-2">
          <h4>Search Reddit</h4>
          <form id="search-form">
            <div className="form-group">
              <input
                type="text"
                id="search-input"
                className="form-control mb-3"
                placeholder="Search Term..."
              />
            </div>
            <div className="form-check form-check-inline">
              Sort By:
              <input
                className="form-check-input ml-2"
                type="radio"
                name="sortby"
                value="relevance"
                checked
              />
              <label className="form-check-label">Relevance</label>
            </div>
            <div className="form-check form-check-inline">
              <input
                className="form-check-input"
                type="radio"
                name="sortby"
                value="new"
              />
              <label className="form-check-label">Latest</label>
            </div>
            <h5 className="mt-2">Limit: </h5>
            <div className="form-group">
              <select name="limit" id="limit" className="form-control">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="25" selected>25</option>
                <option value="50">50</option>
                <option value="100">100</option>
              </select>
            </div>
            <button type="submit" className="btn btn-dark btn-block mt-4">
              Search
            </button>
          </form>
        </div>
        <div  id="results"></div>
      </div>
    </div>
  );
};

export default Reddit;
