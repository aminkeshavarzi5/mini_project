import React from 'react'
import { makeStyles } from "@material-ui/core/styles";
import { useSelector } from "react-redux";


const Footer = () => {
  const classes = useStyles();
  const widthProp = useSelector(state => state.width.width);
  return (
    <div className={classes.container} style={{marginBottom: widthProp> 900 ? 0 : 75 }} >
      Footer
    </div>
  )
}
const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    width: "100%",
    height: 132,
    borderTop:'2px solid black'
  },

}));

export default Footer
