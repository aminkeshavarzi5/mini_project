import React from 'react'
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from '@material-ui/core/CircularProgress';

const Posts = ({posts , loading}) => {
  const classes = useStyles();

  if(loading) {
    return  <div style={{height:"calc(100vh - 380px)", width:'100%',display:'flex',alignItems:'center',justifyContent:'center'}}>
    <CircularProgress style={{color:"#ff9800"}} />
</div>
  }
  
  return (
    <ul className={classes.ul}>
     {posts.map((post) =>{
       return (
       <li className={classes.listItem} key={post.id} >
         {post.title}
       </li>
     )})}
    </ul>

  )
}


const useStyles = makeStyles(theme => ({
ul :{
  listStyleType:'none'
},
listItem:{
  marginTop:10,
  fontSize:'1.3em',
  border:'1px solid black',
  padding:'10px',
  borderRadius:10,
  backgroundColor:'white'
}
}));


export default Posts
