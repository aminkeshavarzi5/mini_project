import React from 'react'
import { makeStyles } from "@material-ui/core/styles";


const Pagination = ({ postsPerPage , totalPosts , paginate}) => {
  const classes = useStyles();
  const pageNumbers = [];

  for(let i=1;i<=totalPosts/postsPerPage;i++){
    pageNumbers.push(i);
  }

  return (
    <ul className={classes.ul}>
      
     
        {pageNumbers.map((number) => ( 
        
        <a onClick={()=> paginate(number)} href="#!" className={classes.link}>
            <li key={number} className={classes.listItem} >
                {number}
            </li>
            </a>
        ))}

    </ul>
  )
}

const useStyles = makeStyles(theme => ({
  ul :{
    listStyleType:'none',
    display:'flex',
    justifyContent:'center',
    width:'50%',
    marginTop:15

  },
  listItem:{
    // marginTop:10,
    // fontSize:'1.3em',
    border:'1px solid black',
    borderRadius:'5px',
    padding:'15px',
    
    
  },
  link:{
    textDecoration:'none',
    color:"black",
    "&:hover" : {
      color:'white',
      backgroundColor:'black'
    },
    margin:'0px 3px'

  }
  }));


export default Pagination
