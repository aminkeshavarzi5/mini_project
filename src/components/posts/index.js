import React , {useState , useEffect} from 'react'
import axios from 'axios'
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from '@material-ui/core';
import Posts from './posts'
import Paginations from './paginations'



const MainPost = () => {

const classes = useStyles();
const [posts, setPosts] = useState([]);
const [loading, setLoading] = useState(false);
const [currentPage, setCurrentPage] = useState(1)
const [postPerPage] = useState(10)

useEffect(() => {
  const fetchPosts = async () => {
    setLoading(true);
    const response = await axios.get("http://jsonplaceholder.typicode.com/posts")
    setPosts(response.data);
    setLoading(false);
  }

  fetchPosts();
}, [])

  //Get current posts 
  const indexOfLastPost = currentPage * postPerPage;
  const indexOfFirstPost = indexOfLastPost - postPerPage;
  const currentPosts = posts.slice(indexOfFirstPost,indexOfLastPost);

  
  let maxPages = posts.length / postPerPage

  const paginateMines = () => {
    while (currentPage !== 1) {
      return setCurrentPage( currentPage-1)
    }
    };
  const paginatePlus = () => {
    while (currentPage !== maxPages) {
      return setCurrentPage(currentPage+1);
    }
    };
    
  
 
  return (
   <div>
     <div className={classes.container}>
      <Typography className={classes.titleName} >My Blog</Typography>
      <Posts posts={currentPosts} loading={loading} />
      <Paginations paginateMines={paginateMines} paginatePlus={paginatePlus} indexOfLastPost={indexOfLastPost} indexOfFirstPost={indexOfFirstPost} />
     </div>
   </div>
  )
}

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    width: "100%",
   
    
  },
  titleName:{
    fontSize:'2em',
    fontWeight:'bold',
    marginTop:30
  }
  }));

export default MainPost
