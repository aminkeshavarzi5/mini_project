import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import ArrowBack from "@material-ui/icons/ArrowBackIos";
import ArrowForward from "@material-ui/icons/ArrowForwardIos";
import { Typography } from "@material-ui/core";
import { useSelector } from "react-redux";

const Pagination = ({
  paginateMines,
  indexOfLastPost,
  indexOfFirstPost,
  paginatePlus
}) => {
  const classes = useStyles();
  const direction = useSelector(state => state.langDir.direction);


  return (
    <div className={classes.container}>
      {direction === "rtl" ? (
        <ArrowForward onClick={() => paginatePlus()} />
      ) : (
        <ArrowBack onClick={() => paginateMines()} />
      )}

      <Typography>
        {" "}
        {indexOfFirstPost}-{indexOfLastPost}{" "}
      </Typography>
      {direction === "rtl" ? (
        <ArrowBack onClick={() => paginateMines()} />
      ) : (
        <ArrowForward onClick={() => paginatePlus()} />
      )}
    </div>
  );
};

const useStyles = makeStyles(theme => ({
  container: {
    listStyleType: "none",
    display: "flex",
    justifyContent: "center",
    width: "50%",
    marginTop: 15
  }
}));

export default Pagination;
