import React, { useState } from 'react';
import Login from './login'
import Register from './register'
import { Button } from '@material-ui/core';
import {loadColorApi} from './../../redux/actions'
import { useSelector, useDispatch } from 'react-redux'
import { makeStyles } from "@material-ui/core/styles";


const Users = () => {
    const [changePage, setChangePage] = useState(false);
    const colorApi = useSelector(state => state.colorApi.color)
    const dispatch = useDispatch()
    const classes = useStyles();

    if (changePage === false) {
        return (
            <div style={{backgroundColor:`${colorApi }`}} className={classes.container}>
                <Login />
                <div className={classes.btns}>
                    <Button style={{ marginLeft: 10 }} className={classes.Btn} onClick={() => setChangePage(!changePage)}>
                        create account
                </Button>
                    <div className={classes.bottomBtns}>
                        <Button onClick={() => { dispatch(loadColorApi()) }} className={classes.Btn} style={{ marginLeft: 10 }} variant="contained">
                            Change Color
                </Button>
                    </div>
                </div>
            </div>
        )
    } else if (changePage === true) {
        return (
            <div style={{backgroundColor:`${colorApi }`}} className={classes.container}>
                <Register />
                <div className={classes.btns} >
                    <Button style={{marginLeft:10}} className={classes.Btn} onClick={() => setChangePage(!changePage)}>
                        login now!
                     </Button>
                    <div className={classes.bottomBtns}>
                        <Button onClick={() => { dispatch(loadColorApi()) }}  className={classes.Btn} style={{ marginLeft: 10 }} variant="contained">
                            Change Color
                         </Button>
                    </div>
                </div>
            </div>
        )
    }

}


const useStyles = makeStyles(theme => ({
    container:{
        width:"100%",
        display:'flex',
        flexDirection:'column',
        justifyContent:'center',
        alignItems:'center'

    },
    btns:{
        display:'flex',
        width:400,
        justifyContent:'center',
        alignItems:'center',
        margin:'10px 0px 50px 0px'
    },
    Btn: {
        borderRadius: "30px",
        backgroundColor: "#8b6797",
        color: "white",
        marginTop: 20,
        minHeight: 40,
        minWidth: 120,
        fontSize:'0.7em',
        boxShadow: "0px 5px 10px 3px rgba(0,0,0,0.75)",
        "&:hover": {
          backgroundColor: "black"
        }
      },
  }));

export default Users;