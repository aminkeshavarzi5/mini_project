import React, { useState } from "react";
import teddy from "./assets/Teddy.flr";
import { makeStyles } from "@material-ui/core/styles";
import FlareComponent from "flare-react";
import { TextField, Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { useSelector, useDispatch } from 'react-redux'
import axios from 'axios'
import { getToken } from './../../redux/actions'




// const handlePasswordConfirm = (value) => {
//   setAnimation("test");
//   if (/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/.test(value)) {
//     setPassword(value);
//     setPasswordError('');

//     return;
//   } else {
//     setPasswordError(lang ? "تایپ پسورد غلط است!" : "password type is incorrect!");
//   }
// }


const Login = () => {
  const [animation, setAnimation] = useState("idle");
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [userNameError, setUserNamerror] = useState('')
  const [passwordError, setPasswordError] = useState('')
  const [submitError, setSubmitError] = useState('')

  const dispatch = useDispatch()
  const classes = useStyles();
  // const darkMode = useSelector(state => state.theme.theme)
  const colorApi = useSelector(state => state.colorApi.color);
  const token = useSelector(state => state.token.token);
  const lang = useSelector(state =>
    state.langDir.language === "FA" ? true : false
  );

  const handleUserNameConfirm = (value) => {
    setAnimation("test");
    if (/^(?=.{4,})/.test(value)) {
      setUserName(value)
      setUserNamerror("");
      return;
    } else {
      setUserNamerror(lang ? "حداقل باید ۴ کاراکتر باشد" : "email type is incorrect!");

    }

  }

  const handlePasswordConfirm = (value) => {
    setAnimation("test");
    if (/^(?=.{4,})/.test(value)) {
      setPassword(value);
      setPasswordError('');

      return;
    } else {
      setPasswordError(lang ? "تایپ پسورد غلط است!" : "password type is incorrect!");
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();

    // console.log('password', password);
    // console.log('userName', userName);
    if (userName === "" || password === "") {
      setSubmitError(lang ? "یکی از فیلد ها خالی است" : "fill the textFields")
    }
    else if (passwordError !== "" && userNameError !== "") {
      submitError("fix the errors!")
    }
    else {

      axios.post('http://localhost:8888/wp-json/simple-jwt-authentication/v1/token', {
        username: userName,
        password: password
      })
        .then((res) => {
          dispatch(getToken(res.data.token))
        })
        .catch((error) => {
          console.log(error);
        });

    }

  }
  if (!!token) return <p>u r loged in!</p>
  return (
    <div style={{ backgroundColor: `${colorApi}` }} className={classes.container}>
      <div className={classes.teddyBox}>
        <FlareComponent
          className={classes.teddy}
          width={300}
          height={300}
          animationName={animation}
          file={teddy}
        />

        <form onSubmit={(e) => handleSubmit(e)} className={classes.loginBox}>
          <TextField
            defaultValue={userName}
            onChange={(e) => handleUserNameConfirm(e.target.value)}
            className={classes.txtFeilds}
            id="standard-search"
            label="username"
            type="search"
          />
          <Typography style={{ color: 'red' }}>{userNameError}</Typography>

          <TextField
            error={passwordError !== "" ? true : false}
            defaultValue={password}
            onChange={(e) => handlePasswordConfirm(e.target.value)}
            className={classes.txtFeilds}
            style={{ marginTop: 20 }}
            id="standard-password-input"
            label="Password"
            type="password"
            autoComplete="current-password"
          />
          <Typography style={{ color: 'red' }}>{passwordError}</Typography>
          <Button
            onClick={() => {
              setAnimation("success");
            }}
            onDoubleClick={() => {
              setAnimation("fail");
            }}
            type="submit"
            className={classes.loginBtn}
            variant="contained"
          >
            Login
          </Button>
        </form>
      </div>

    </div>
  );
};

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    width: "100%",
  },
  teddyBox: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    alignItems: "center",
    position: "relative",
    height: "70%",
    marginTop: 30

  },
  teddy: {
    borderRadius: "40px"
  },
  loginBox: {
    display: "flex",
    flexDirection: "column",
    minWidth: 400,
    padding: "40px 10px",
    zIndex: 1,
    marginTop: '-50px',
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: "30px",
    boxShadow: "0px 5px 15px 3px rgba(0,0,0,0.75)",
  },
  txtFeilds: {
    width: "90%"
  },
  loginBtn: {
    borderRadius: "30px",
    backgroundColor: "#8b6797",
    color: "white",
    marginTop: 20,
    minHeight: 40,
    minWidth: 120,
    width: "80%",
    boxShadow: "0px 5px 10px 3px rgba(0,0,0,0.75)",

    "&:hover": {
      backgroundColor: "black"
    }
  },
  bottomBtns: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
    // alignItems: "center",
    // height: "30%"
    marginBottom: 170
  },
  btmBtn: {
    borderRadius: "30px",
    backgroundColor: "#8b6797",
    color: "white",
    marginTop: 20,
    minHeight: 40,
    width: 150,
    fontSize: '0.7em',
    boxShadow: "5px 5px 8px rgba(0,0,0,0.75)",
    "&:hover": {
      backgroundColor: "black"
    }
  }
}));

export default Login;
