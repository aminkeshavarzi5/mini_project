import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Button, TextField, Typography } from "@material-ui/core";
import { useSelector, useDispatch } from "react-redux";
import Snackbar from "@material-ui/core/Snackbar";
import { useGoogleReCaptcha } from "react-google-recaptcha-v3";
import Axios from "axios";
import { getToken } from "./../../redux/actions";
import { withRouter } from "react-router-dom";

const Register = ({ history }) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  const [userName, setUserName] = useState("amin keshavarzi");
  const [name, setName] = useState("amin1375");
  const [email, setEmail] = useState("amin@gmail.com");
  const [password, setPassword] = useState("Amin1375");
  const [rePassword, setRePassword] = useState("Amin1375");
  const [nameError, setNameError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [rePasswordError, setRePasswordError] = useState("");
  const [userNameError, setUserNameError] = useState("");
  const [submitError, setSubmitError] = useState("");
  const { executeRecaptcha } = useGoogleReCaptcha();
  const [snackOpen, setSnackOpen] = useState({
    open: false,
    vertical: "bottom",
    horizontal: "left"
  });

  const { vertical, horizontal, open } = snackOpen;

  const handleClose = () => {
    setSnackOpen({ ...snackOpen, open: false });
  };

  const lang = useSelector(state =>
    state.langDir.language === "FA" ? true : false
  );

  const handleChangeName = value => {
    if (!/[0-9]+$/.test(value)) {
      setNameError("");
      setName(value);
    } else {
      setNameError(lang ? "!عدد نمیتواند وارد شود" : "Can't Accept Numbers!");
    }
  };
  const handleChangeUserName = value => {
    if (/^(?=.{4,})/.test(value)) {
      setUserNameError("");
      setUserName(value);
    } else {
      setUserNameError(lang ? "حداقل ۴ حرف وارد کنید" : "atleast 4 digits");
    }
  };

  const handleChangeEmail = value => {
    if (/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(value)) {
      setEmailError("");
      setEmail(value);
    } else {
      setEmailError(
        lang ? "فرمت ایمیل اشتباه است!" : "your email is incorrect"
      );
    }
  };
  const handleChangePassword = value => {
    if (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,32}$/.test(value)) {
      setPasswordError("");
      setPassword(value);
    } else {
      setPasswordError(
        lang
          ? " پسورد باید حداقل دارای 8 کاراکتر حاوی حروف بزرگ انگلیسی و اعداد باشد !"
          : "your password is incorrect"
      );
    }
  };
  const handleChangeRePassword = value => {
    if (value === password) {
      setRePasswordError("");
      setRePassword(value);
    } else {
      setRePasswordError(
        lang
          ? " مقدار وارد شده همخوانی ندارد!"
          : "the value is not match with Password!"
      );
    }
  };

  const handleSubmit = async e => {
    e.preventDefault();
    if (
      userName === "" ||
      email === "" ||
      name === "" ||
      (password === "") | (rePassword === "")
    ) {
      setSubmitError(lang ? "یکی از فیلد ها خالی است" : "fill the textFields");
      setSnackOpen({
        open: true,
        ...{ vertical: "bottom", horizontal: "left" }
      });
    } else if (
      !!userNameError ||
      !!nameError ||
      !!passwordError ||
      !!emailError ||
      !!rePasswordError
    ) {
      setSubmitError("fix the errors!");
    } else {
      if (!executeRecaptcha) {
        return;
      }
      const result = await executeRecaptcha("register");
      try {
        const response = await Axios.post(
          "http://localhost:8888/wp-json/wp/v2/users/register",
          {
            username: userName,
            fullName: name,
            email: email,
            password: password,
            gRecaptchaResponse: result,
            role: "subscriber"
          }
        );
        if (response && response.data && response.data.code === 200) {
          dispatch(getToken(response.data.token));
          history.push("/");
        } else {
          console.log("response ", response);
          setSubmitError("ridi");
          setSnackOpen({
            open: true,
            ...{ vertical: "bottom", horizontal: "left" }
          });
        }
      } catch (error) {
        if (error.response.data.code === 451) {
          setSubmitError("email is already exists");
          setSnackOpen({
            open: true,
            ...{ vertical: "bottom", horizontal: "left" }
          });
        } else if (error.response.data.code === 450) {
          setSubmitError("username is already exists");
          setSnackOpen({
            open: true,
            ...{ vertical: "bottom", horizontal: "left" }
          });
        }
      }
      setTimeout(() => {
        handleClose();
      }, 4000);
    }
  };

  return (
    <div onSubmit={handleSubmit} className={classes.container}>
      <form className={classes.regBox}>
        <div className={classes.textFields}>
          <TextField
            className={classes.txt}
            onChange={e => handleChangeName(e.target.value)}
            defaultValue={name}
            id="standard-basic"
            label="FullName"
          />
          <Typography style={{ color: "red" }}>{nameError}</Typography>

          <TextField
            className={classes.txt}
            onChange={e => handleChangeUserName(e.target.value)}
            defaultValue={userName}
            id="standard-basic"
            label="username"
          />
          <Typography style={{ color: "red" }}>{userNameError}</Typography>

          <TextField
            className={classes.txt}
            onChange={e => handleChangeEmail(e.target.value)}
            defaultValue={email}
            id="standard-basic"
            label="email"
          />
          <Typography style={{ color: "red" }}>{emailError}</Typography>

          <TextField
            type="password"
            className={classes.txt}
            onChange={e => handleChangePassword(e.target.value)}
            defaultValue={password}
            id="standard-basic"
            label="password"
          />
          <Typography style={{ color: "red" }}>{passwordError}</Typography>

          <TextField
            type="password"
            className={classes.txt}
            onChange={e => handleChangeRePassword(e.target.value)}
            defaultValue={rePassword}
            id="standard-basic"
            label="repeat password"
          />
          <Typography style={{ color: "red" }}>{rePasswordError}</Typography>
          <Typography style={{ width: "80%" }}>
            This site is protected by reCAPTCHA and the Google
            <br />
            <a href="https://policies.google.com/privacy">Privacy Policy</a> and
            <a href="https://policies.google.com/terms">
              Terms of Service
            </a>{" "}
            apply.
          </Typography>
        </div>
        <Button type="submit" className={classes.submittBtn}>
          Register
        </Button>
      </form>
      <Snackbar
        anchorOrigin={{ vertical, horizontal }}
        key={`${vertical},${horizontal}`}
        open={open}
        onClose={handleClose}
        message={submitError}
      />
    </div>
  );
};

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    width: "100%"
  },
  regBox: {
    minWidth: 500,
    maxWidth: 550,
    padding: 15,
    height: "auto",
    marginTop: 30,
    backgroundColor: "white",
    display: "flex",
    flexDirection: "column",
    boxShadow: "0px 5px 10px 3px rgba(0,0,0,0.75)",
    borderRadius: "30px",
    // justifyContent:'center',
    alignItems: "center"
  },
  textFields: {
    display: "flex",
    flexDirection: "column",
    marginTop: 25,
    width: "80%"
  },
  txt: {
    marginBottom: 15
  },
  submittBtn: {
    boxShadow: "0px 5px 5px 3px rgba(0,0,0,0.75)",
    marginTop: 10,
    borderRadius: 30,
    fontSize: "0.7em",
    // fontWeight:"bold",
    backgroundColor: "#8b6797",
    "&:hover": {
      background: "black",
      color: "white"
    }
  }
}));

export default withRouter(Register);
