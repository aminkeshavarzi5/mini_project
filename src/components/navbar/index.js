import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Box, Button } from "@material-ui/core";
import SyncAltIcon from "@material-ui/icons/SyncAlt";
import { Link } from "react-router-dom";
import Background from "./assets/navBack.jpg";
import { useSelector } from "react-redux";
import { changeLanguage } from "./../../redux/actions";
import { useDispatch } from "react-redux";
import { changeTheme } from "./../../redux/actions";
import Sun from '@material-ui/icons/Brightness7';
import Moon from '@material-ui/icons/Brightness4';
import reddit from './assets/reddit.png'

const Navbar = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const path = useSelector(state => state.url.url);
  const direction = useSelector(state => state.langDir.direction);
  const lang = useSelector(state =>
    state.langDir.language === "FA" ? true : false
  );
  const darkMode = useSelector(state => state.theme.theme)

  const widthProp = useSelector(state => state.width.width);
  document.body.dir = direction;

  if (widthProp > 900) {
    return (
      <div className={classes.container}>
        <Box className={classes.navbar}>
          <ul
            className={classes.ulNav}
            style={{ listStyleType: "none", padding: 0 }}
          >
            <Link to="/">
              <li className={path === "/" ? classes.marked : classes.navBtns}>
              {lang ?"خانه"  : "Home"}   
              </li>
            </Link>
            <Link to="/article">
              <li
                className={
                  path === "/article" ? classes.marked : classes.navBtns
                }
              >
               {lang ?"مقاله"  : "Article"}  
              </li>
            </Link>
            <Link to="/contact">
              <li
                className={
                  path === "/contact" ? classes.marked : classes.navBtns
                }
              >
               {lang ?"ارتباط با ما"  : "Contact"} 
              </li>
            </Link>
            <Link to="/about">
              <li
                className={path === "/about" ? classes.marked : classes.navBtns}
              >
                {lang ?"درباره ما"  : "About"}
              </li>
            </Link>
          </ul>
          <div className={classes.optionBtns}>
            <Button
              style={{ marginLeft: lang === "FA" ? 0 : 5 }}
              onClick={() => dispatch(changeLanguage())}
              className={classes.langBtn}
              color="secondary"
            >
              <SyncAltIcon style={{ marginLeft: 3 }} />
             {lang ?"eng"  : "فارسی"}
            </Button>
            <Button
              style={{ marginLeft: lang === "FA" ? 5 : 0 }}
              onClick={() => dispatch(changeTheme())}
              className={classes.darkBtn}
              color="secondary"
            >
              {darkMode==="dark" ? <Sun/> : <Moon/> }
            </Button>
                <Link to="/reddit">
              <img src={reddit} alt="reddit" className={classes.reddits} />
               </Link>
               <Link to="/users">
               Login
               </Link>
          </div>
        </Box>
      </div>
    );
  } else {
    return (
    <div className={classes.bottomNavigation}>
      <ul className={classes.ulNavBtm}>
      <a href="/"   style={{width:'25%'}}>
        <li className={
                  path === "/home" ? classes.marked : classes.navBtns
                }>Home</li>
                </a>
                <a  href="/article" style={{width:'25%'}} >
        <li className={
                  path === "/article" ? classes.marked : classes.navBtns
                }>Article</li>
                </a>
                <a  href="/contact" style={{width:'25%'}} >
        <li className={
                  path === "/contact" ? classes.marked : classes.navBtns
                }>Contact</li>
                </a>
                <a href="/about" style={{width:'25%'}} >
        <li className={
                  path === "/about" ? classes.marked : classes.navBtns
                }>About</li>
                </a>
      </ul>
      
    </div>
    );
  }
}

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    zIndex: 2,
    minHeight: 120,
    width: "100%",
    backgroundImage: `url( ${Background})`,
    boxShadow: "0px 10px 10px rgba(0, 0, 0, 1)"
  },
  links:{
    width:"25%",
    display:'flex'
  },
  bottomNavigation:{
      width:'100%',
      display:'flex',
      minHeight:75,
      position:'fixed',
      bottom:0,
      right:0,
      backgroundImage: `url( ${Background})`,
      zIndex: 2,
  },
  ulNavBtm:{
    justifyContent:'space-between',
    display:'flex',
    listStyleType: "none",
    padding:'0px 30px',
    width:'100%',
    color:'white',
    alignItems:'center'
  },
  navBtns: {
    marginRight: 10,
    fontSize: "0.7em",
    width:"100%",
    "&:hover": {
      color: "black",
      fontSize: "0.9em",
      backgroundColor: "#ff9800"
    },
    color: "white",
    borderRadius: "40px",
    /* text-decoration: none; */
    
    listStyleType: "none",
    padding: "5px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  marked: {
    marginBottom: 5,
    backgroundColor: "white",
    fontSize: "0.9em",
    marginRight: 10,
    color: "black",
    borderRadius: "40px",
    /* text-decoration: none; */
    width:"100%",
    listStyleType: "none",
    padding: "5px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  navbar: {
    display: "flex",
    width: "90%",
    margin: "auto",
    justifyContent: "space-between",
    alignItems: "center"
  },
  ulNav: {
    display: "flex",
    fontSize: "1.5em",
    width: "60%",
    justifyContent: "space-between",
    alignItems:'center'
  },
  optionBtns: {
    width: "60%",
    display: "flex",
    alignItems: "center",
    flexDirection: "row-reverse"
    // justifyContent:'space-between'
  },
  btns: {
    color: "white",
    fontSize: "0.7em",
    
    "&:hover": {
      color: "black",
      backgroundColor: "#c2185b"
    }
  },
  langBtn: {
    color: "white",
    width: "100px",
    "&:hover": {
      // color:'black',
      backgroundColor: "#5d8e9b",
      border: "1px solid white",
      borderRadius: "30px"
    }
  },
  darkBtn:{
    color: "white",
    width: "80px",
    "&:hover": {
      backgroundColor:'transparent'
    }
  },
  reddits:{
    width:40,
    height:40,
    marginRight:10,
    "&:hover" :{
      width:50,
      height:50,

    }
   
    
  }
}));

export default Navbar;
