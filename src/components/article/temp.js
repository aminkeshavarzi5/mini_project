export default {
  posts: [
    {
      id: 1,
      userPhoto: require("./assets/1.jpg"),
      title: "Shrimp and Chorizo Paella",
      date: "September 14, 2016",
      text:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      likes: "200",
      retwitt: "35"
    },
    {
      id: 2,
      userPhoto: require("./assets/2.jpg"),
      title: "Shrimp and Chorizo Paella",
      date: "September 14, 2016",
      text:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      likes: "60",
      retwitt: "10"
    },
    {
      id: 3,
      userPhoto: require("./assets/3.jpg"),
      title: "Shrimp and Chorizo Paella",
      date: "September 14, 2016",
      text:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      likes: "70",
      retwitt: "35"
    },
    {
      id: 4,
      userPhoto: require("./assets/4.jpg"),
      title: "Shrimp and Chorizo Paella",
      date: "September 14, 2016",
      text:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      likes: "150",
      retwitt: "25"
    },
    {
      id: 5,
      userPhoto: require("./assets/5.jpg"),
      title: "Shrimp and Chorizo Paella",
      date: "September 14, 2016",
      text:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      likes: "50",
      retwitt: "20"
    },
    {
      id: 6,
      userPhoto: require("./assets/6.jpg"),
      title: "Shrimp and Chorizo Paella",
      date: "September 14, 2016",
      text:
        "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      likes: "100",
      retwitt: "80"
    }
  ]
};
