import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography, Button } from "@material-ui/core";
import { useSelector } from "react-redux";
import Posts from "./posts";
import postsInfo from "./temp";

const Article = () => {
  const classes = useStyles();

  const lang = useSelector(state => (state.langDir.language === "FA" ? true : false));
  return (
    <div className={classes.container}>
      <div className={classes.title}>
      <Typography className={classes.txtTitle} >{lang ? "پست های توییتر" : "posts from twiter"}</Typography>
      <a href="/posts"><Button className={classes.postBtn}>{lang ? "پست ها" : "posts"}</Button></a>
      </div>
      <div className={classes.postsFather}>
        {postsInfo.posts.map(post => {
          return (
            <Posts
              key={post.id}
              userPhoto={post.userPhoto}
              title={post.title}
              date={post.date}
              text={post.text}
              likes={post.likes}
              retwitt={post.retwitt}
            />
          );
        })}
      </div>
    </div>
  );
};

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    width: "100%"
  },
  title: {
    width: "80%",
    display: "flex",
    margin:50,
    justifyContent:'space-between',
    

    // fontSize: "2.5em",
    // padding: "30px 30px 0px 0px "
  },
  postBtn:{
    width:'100px',
    height:'45px',
    fontSize:'1.3em',
    color:'white',
    backgroundColor:'black',
    borderRadius:'30px',
    "&:hover" : {
      backgroundColor:"#ff9800"
      ,color:'black'
    }
  },
  txtTitle:{
    fontSize:'2.2em'
  },
  postsFather: {
    width: "80%",
    display: "grid",
    gridGap: 10,
    justifyContent:'center',
    alignItems:'center',
    gridTemplateColumns: 'repeat(5,minmax(auto,300px))',
    marginTop: 50,
    [theme.breakpoints.down('1930')]: {
      
      gridTemplateColumns: 'repeat(3,minmax(auto,300px))',
    },
    [theme.breakpoints.down('1162')]: {
      
      gridTemplateColumns: 'repeat(2,minmax(auto,300px))',
    },
    [theme.breakpoints.down('778')]: {
      
      gridTemplateColumns: 'repeat(1,minmax(auto,300px))',
    },
    marginBottom:30
    
  }
}));

export default Article;
